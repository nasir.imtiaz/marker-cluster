# Marker Cluster
Project to organize and visualize several cluttered and overlapping markers in a clustered manner.

This is an application to demonstrate clustering large amount of data points on the map. In case there are more than one nearby points within a proximity of 100px (customizable), a cluster is formed. This way the data points do not appear overlapped over each other and provides better visualisation. Zooming in or clicking a cluster, explodes the cluster into sub-clusters (in case there still exist multiple nearby markers) or displays the actual data point. For data points that are too close to each other even after a maximum zoom, clicking the cluster would display the contained data points.

In order to implement clustering of markers, the Leaflet Marker Cluster plugin of the open source Leaflet JavaScript library has been used.
### Features
- It is easy to integrate
- Easy to customize (e.g. can customize cluster marker icon and individual markers)
- Groups overlapping or nearby markers into a cluster
- Supports mobile and desktop platforms
- Supports all browsers
- Has sub-plugins for additional functionalities e.g. to display child elements in a list when a cluster is clicked.

### How to use:
  - You need to access the main page `cluster.html` in your browser where you can
	- Specify a geojson URL to populate data points on the map.
	- Specify map dimensions to increase or decrease the `width` and `height` of the map
	- Specify icon dimensions to increase or decrease the `width` and `height` of the data point marker
	- Change the map coordinate bounding box by specifying bounding box corners using _[Lower left latitude, Lower left longitude]_ and _[Top right latitude, Top right longitude]_