var mymap = L.map('mapid').setView([51.507257, -0.147848], 13);

mymap.fitBounds([
  [51.4904, -0.27603],
  [51.6836, 0.05081]
]);

L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
 attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
 subdomains: ['a','b','c']
}).addTo( mymap );

var markerClusters = L.markerClusterGroup({
  spiderfyOnMaxZoom: true, //When you click a cluster at the bottom zoom level we spiderfy it so you can see all of its markers. 
  spiderLegPolylineOptions: { weight: 2.5, color: '#FF0000', opacity: 0.5 }, //Allows you to specify PolylineOptions to style spider legs.
  showCoverageOnHover: false, //When you mouse over a cluster it shows the bounds of its markers.
  zoomToBoundsOnClick: true, //When you click a cluster we zoom to its bounds
  chunkedLoading: true, //Boolean to split the addLayers processing in to small intervals so that the page does not freeze
  maxClusterRadius: 100, //A cluster will cover at most this many pixels from its center
  removeOutsideVisibleBounds: true //Clusters and markers too far from the viewport are removed from the map for performance.
});


function onEachFeature(feature, layer) {
  if (feature.properties) {
      var popupContent = '';

      $.each( feature.properties , function( key, value ) {
        popupContent = popupContent+'<strong>'+key+': </strong>'+value+'</br>';
      });
      
      popupContent = popupContent+'<strong>Latitude: </strong>'+feature.geometry.coordinates[1]+'</br>';
      popupContent = popupContent+'<strong>Longitude: </strong>'+feature.geometry.coordinates[0]+'</br>';

      layer.bindPopup(popupContent).openPopup();
  }

  markerClusters.addLayer(layer);
}

var myIcon = L.icon({
  iconUrl: 'images/marker.png',
  iconSize: [25, 40]
});

function renderData(dataUrl, changeBound = true){
  markerClusters.clearLayers();
  
  $.getJSON(dataUrl, function(geojsonFeature) {
	  var geoJsonLayer = L.geoJSON(geojsonFeature, {
		pointToLayer: function (feature, latlng) {
		  return L.marker(latlng, {icon: myIcon});
		},
		onEachFeature: onEachFeature
	  });
	  
    if (changeBound)
	   mymap.fitBounds(geoJsonLayer.getBounds()); 
  });
}

  mymap.addLayer( markerClusters );
  
  $('#mapReload').on('click', function(){
    console.log('Reload Map');
    $('#mapid').width($('#mapWidth').val());
    $('#mapid').height($('#mapHeight').val());

    myIcon.options.iconSize = [$('#iconWidth').val(), $('#iconHeight').val()];

    mymap.fitBounds([
    	[$('#lowerLeftLatitude').val(), $('#lowerLeftlongitude').val()],
    	[$('#topRightLatitude').val(), $('#topRightlongitude').val()]
    ]);
  
    renderData($('#data-url').val(), false);
  });

$('#renderData').on('click', function(){
  console.log('Render Data');
  renderData($('#data-url').val());
});

$( document ).ready(function() {
  setTimeout(function(){ $('#renderData').click(); }, 500);
});